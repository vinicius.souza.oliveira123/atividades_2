/*
Faça um programa que recebe um número inteiro e verifique se esse número é par ou ímpar.
*/ 

namespace exercicio_4
{
    let numero1, numero2, resultado1, resultado2: number

    numero1 = 2
    numero2 = 5

    resultado1 = numero1 % 2

    resultado2 = numero2 % 2

    if(resultado1 == 0)
    {
        console.log("O numero é par")
    }
    else 
    {
        console.log("O numero é impar")
    }

    if (resultado2 == 0)
    {
        console.log("O numero é par")
    }

    else
    {
        console.log("O numero é impar")
    }
}