//testando o operador OU - ||
namespace operadorOu 
{
    let idade = 19;


    let maiorIdade = idade > 18;

    let possuiAutorizacaoDosPais = false;

    let podeBeber = maiorIdade || possuiAutorizacaoDosPais;

    console.log(podeBeber); //true
}